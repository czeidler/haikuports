SUMMARY="Cross platform Make"
DESCRIPTION="
CMake is a cross-platform, open-source build system. It is a family of tools \
designed to build, test and package software. CMake is used to control the \
software compilation process using simple platform and compiler independent \
configuration files. CMake generates native makefiles and workspaces that can \
be used in the compiler environment of your choice.
"
HOMEPAGE="http://www.cmake.org" 
LICENSE="CMake"
COPYRIGHT="2002-2013 Kitware, Inc., Insight Consortium, All rights reserved."
SRC_URI="http://www.cmake.org/files/v3.0/cmake-3.0.0-rc1.tar.gz"
CHECKSUM_SHA256="f13ac75c2440cb81aa1c193cf4f3898092f72a0c2d43b7b02f8476a8e5e1d1e6"
REVISION="7"
ARCHITECTURES="x86 x86_64 arm"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	cmake = $portVersion compat >= 3.0
	cmd:cmake = $portVersion compat >= 3.0
	cmd:ccmake = $portVersion compat >= 3.0
	cmd:cpack = $portVersion compat >= 3.0
	cmd:ctest = $portVersion compat >= 3.0
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libncurses$secondaryArchSuffix
	"
BUILD_REQUIRES="
	devel:libncurses$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	haiku_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:grep
	cmd:ld$secondaryArchSuffix
	cmd:libtool
	cmd:make
	cmd:sed
	"
PATCHES="cmake-$portVersion.patchset"
SOURCE_DIR="cmake-3.0.0-rc1"
BUILD()
{
	./configure --prefix=$prefix \
		--datadir=/$relativeDataDir/cmake \
		--docdir=/$relativeDocDir \
		--mandir=/$relativeManDir
	make $jobArgs
}

INSTALL()
{
	make install

	# No way to tell this to configure...
	mv $prefix/share/aclocal $dataDir
	rmdir $prefix/share
}

TEST()
{
	cp bin/ctest Bootstrap.cmk/
	make test VERBOSE=1
}

